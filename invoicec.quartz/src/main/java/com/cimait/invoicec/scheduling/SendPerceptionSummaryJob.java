package com.cimait.invoicec.scheduling;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.cimait.invoicec.entity.DocumentPerception;
import com.cimait.invoicec.entity.Emitter;
import com.cimait.invoicec.service.InvoicecDocintroService;
import com.cimait.invoicec.service.InvoicecProcessBackgroundService;
import com.cimait.invoicec.service.repository.DocumentPerceptionService;
import com.cimait.invoicec.service.repository.EmitterService;
import com.cimait.invoicec.service.repository.SummaryBajaServices;
import com.cimait.invoicec.util.InfoEmpresa;



@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class SendPerceptionSummaryJob extends QuartzJobBean{
	
	
	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException { generatePerceptionSummary();}


	
	public void generatePerceptionSummary() {
		try {
				LOGGER.info("### Ejecutandose JOB de Generacion de Resumen de Reversiones Retenciones");
				LOGGER.info("Hora de Ejecucion: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a").format(new Date()));
				
				List<Emitter> emitters = emitterSrv.findAllEmiters();
				
				for (Emitter emitter : emitters) {
					InfoEmpresa infoEmpresa = docIntroSrv.obtieneInfoEmpresa(emitter.getIdentification());
					List<DocumentPerception> lstFechaEmision = documentPerceptionService.obtenerFechaEmisionDeDocsEP(infoEmpresa.getRuc());
					int correlativo = summaryBajaServices.getCorrelativoSummary(infoEmpresa.getRuc(), "RR");
					
					for (DocumentPerception doc : lstFechaEmision) {
						List<DocumentPerception> documentosBaja = documentPerceptionService.docPendientesDeBaja(doc.getFechaEmision(), doc.getRucEmisor());
						List<DocumentPerception> lstDocs = new ArrayList<DocumentPerception>();
						Date issueDate = null;
						++correlativo;
						for (DocumentPerception documento : documentosBaja) {
							DocumentPerception d = new DocumentPerception();
							issueDate = documento.getFechaEmision();
							d.setNumeroLegal(documento.getNumeroLegal());
							d.setTipoDocumento(documento.getTipoDocumento());
							d.setFechaEmision(documento.getFechaEmision());
							d.setMotivoBaja(documento.getMotivoBaja());
							lstDocs.add(d);
						}
						if(lstDocs.size() > 0){
							docIntroSrv.generarResumenReversionesPercepciones(lstDocs, issueDate, String.format("%05d",correlativo), new Date(), infoEmpresa);
							procesaResumen.procesa(infoEmpresa);
						}
						
						
					}
				}
							
			

		} catch (Exception e)
			{
				LOGGER.error("Ocurrio un error en la ejecucion del JOB de Generacion de Resumen de Percepcioncesiones. \n" +e);
				e.printStackTrace();
			
			}
		
	}	
	
	
	

	@Autowired
	private SummaryBajaServices summaryBajaServices;
	@Autowired
	private InvoicecDocintroService docIntroSrv;
	@Autowired
	private EmitterService emitterSrv;
	@Autowired
	private DocumentPerceptionService documentPerceptionService;
	@Autowired
	private InvoicecProcessBackgroundService procesaResumen;
	private static Logger LOGGER = Logger.getLogger(SendPerceptionSummaryJob.class);
	
	
}
