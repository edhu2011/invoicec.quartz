package com.cimait.invoicec.scheduling;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cimait.invoicec.entity.Document;
import com.cimait.invoicec.entity.Emitter;
import com.cimait.invoicec.repository.DocumentRepository;
import com.cimait.invoicec.service.InvoicecDocintroService;
import com.cimait.invoicec.service.InvoicecOdooService;
import com.cimait.invoicec.service.repository.DocumentService;
import com.cimait.invoicec.service.repository.EmitterService;
import com.cimait.invoicec.util.InfoEmpresa;



@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class SearchDocumentsBajaOdooJob extends QuartzJobBean{
	
	
	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException { generateReceiptSummary(getYesterdayDate());}

	@Transactional
	public void generateReceiptSummary(Date fechaEmisionBoletas) {
		try {
			
			List<Emitter> emitters = emitterSrv.findAllEmiters();
			for (Emitter emitter : emitters) {
				
				if(emitter.getSourceDocument().trim().equalsIgnoreCase("odoo")){			
					
						InfoEmpresa infoEmpresa = docIntroSrv.obtieneInfoEmpresa(emitter.getIdentification());
				
				
						ArrayList<HashMap<String,Object>> lstDocumentoBaja = new ArrayList<HashMap<String,Object>>();				
						invoicecOdooService.documentosBaja(lstDocumentoBaja, infoEmpresa);
						LOGGER.info("### Ejecutandose JOB de Baja de Documentos Odoo");
						LOGGER.info("Hora de Ejecucion de JOB: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a").format(new Date()));
						LOGGER.info("SE ENCONTRARON : "+lstDocumentoBaja.size()+" DOCUMENTOS DE BAJA EN \"ODOO\".");
						
						for (HashMap<String, Object> hashMap : lstDocumentoBaja) {				
							Document doc = documentRepository.findOneDocument((String)hashMap.get("InvoiceNumeracion"), (String)hashMap.get("RucEmisor"), (String)hashMap.get("TipoDocumento"));
							/**TODO ACTUALIZA STATUS DOCUMENTOS YA INFORMADOS A SUNAT - SERIA BAJA DE DOCUMENTO**/
							if(doc != null && doc.getStatus().equals("AT") && doc.getProcesado()){
								doc.setStatus("PB");
								doc.setFechaBajaDocumento(new Date());
								documentRepository.save(doc);
								
							}
							/**TODO ACTUALIZA STATUS DOCUMENTOS PENDIENTE - SERIA ANULACION SIMPLE**/
							if(doc != null && doc.getStatus().equals("PD") && doc.getProcesado()){
								doc.setStatus("AN");
								doc.setFechaBajaDocumento(new Date());
								documentRepository.save(doc);
								
							}
							
						}
					
				}
	
				
			}
			
				LOGGER.info("### Termino JOB de Baja de Documentos Odoo");
				
				
				
		} catch (Exception e)
			{
			LOGGER.error("Ocurrio un error en la ejecucion del JOB de Update Status Documents Odoo. \n" +e);
			e.printStackTrace();
			
			}
		
	}	
	
	
	
	private Date getYesterdayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);    
        return cal.getTime();
	}
	
	@Autowired
	private InvoicecOdooService invoicecOdooService;
	@Autowired
	private InvoicecDocintroService docIntroSrv;
	@Autowired
	private DocumentService documentService;
	@Autowired
	private DocumentRepository documentRepository;	
	@Autowired
	private EmitterService emitterSrv;
	private static Logger LOGGER = Logger.getLogger(SearchDocumentsBajaOdooJob.class);
	
	
}
