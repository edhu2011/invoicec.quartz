package com.cimait.invoicec.scheduling;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import com.cimait.invoicec.entity.Document;
import com.cimait.invoicec.entity.Emitter;
import com.cimait.invoicec.service.InvoicecDocintroService;
import com.cimait.invoicec.service.repository.DocumentService;
import com.cimait.invoicec.service.repository.EmitterService;
import com.cimait.invoicec.util.FileUtil;
import com.cimait.invoicec.util.InfoEmpresa;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class SendPendingInvoicesJob extends QuartzJobBean {

	public void sendPendingInvoices() {
			
			LOGGER.info("### Ejecutandose JOB de Envio de Facturas");
			LOGGER.info("Hora de Ejecucion: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a").format(new Date()));
			List<Emitter> emitters = emitterSrv.findAllEmiters();
			for (Emitter emitter : emitters) {
			
					InfoEmpresa infoEmpresa = docIntroSrv.obtieneInfoEmpresa(emitter.getIdentification());					
					List<Document> docs =  documentService.facturasPendientesQuartz_v2(infoEmpresa.getDiasAtrasFactura(), infoEmpresa.getRuc());
					
					if(docs.size() > 0){
							LOGGER.info("### Cantidad Facturas Pendientes " + docs.size());
							LOGGER.info("### Ruc Emisor " + infoEmpresa.getRuc());
							LOGGER.info("### Name Emisor " + infoEmpresa.getRazonSocial());
							int cont = 0;
							for (Document doc : docs) {
										try {
											cont ++;
											LOGGER.info("### Cantidad de Factura: " + cont);
											LOGGER.info("### Inicio Enviando Factura " + doc.getNumeroLegal());		
											
											FileUtil.setFileName(doc.getNombreArchivo());
											
											if((doc.getTipoDocumento().equals("07") || doc.getTipoDocumento().equals("08")) && doc.getDocumentoModificado().startsWith("F")){
												
												//String servOrder = doc.getServiceOrder() == null ? "" : doc.getServiceOrder();
												Document factura = documentService.obtenerDocumentoModificado(doc.getDocumentoModificado().trim(), "01",doc.getRuc());
												if(factura != null && factura.getStatus().equals("AT")){								
													docIntroSrv.processPendingInvoice(infoEmpresa, doc);
												}else{									
														if (factura != null && !factura.getStatus().equals("AT")){
															docIntroSrv.saveStatusError(doc, "La factura relacionada "+doc.getDocumentoModificado()+" NO esta AUTORIZADA");
															LOGGER.info("La factura relacionada "+doc.getDocumentoModificado()+" NO esta AUTORIZADA");
															
														}
														else{
															docIntroSrv.saveStatusError(doc, "La factura relacionada NO esta REGISTRADA EN LA BD con datos: numero = "+doc.getDocumentoModificado()+", tipo = 01, ruc = "+doc.getRuc());											
															LOGGER.info("La factura relacionada NO esta REGISTRADA EN LA BD con datos: numero = "+doc.getDocumentoModificado()+", tipo = 01, ruc = "+doc.getRuc());
															
														}
												}
											}
											else{
													docIntroSrv.processPendingInvoice(infoEmpresa, doc);
											}
																		
											LOGGER.info("### Fin Enviando Factura " + doc.getNumeroLegal());							
											
											
										} catch (SQLException e) {
											LOGGER.error("Ocurrio un error en la ejecucion del JOB de de Envio de Facturas. \n" +e);
											e.printStackTrace();
										} catch (ParserConfigurationException e) {
											LOGGER.error("Ocurrio un error en la ejecucion del JOB de de Envio de Facturas. \n" +e);
											e.printStackTrace();
										} catch (SAXException e) {
											LOGGER.error("Ocurrio un error en la ejecucion del JOB de de Envio de Facturas. \n" +e);
											e.printStackTrace();
										} catch (IOException e) {
											LOGGER.error("Ocurrio un error en la ejecucion del JOB de de Envio de Facturas. \n" +e);
											e.printStackTrace();
										} catch (Exception e) {
											LOGGER.error("Ocurrio un error en la ejecucion del JOB de de Envio de Facturas. \n" +e);
											e.printStackTrace();
										}
							}
								LOGGER.info("### Cantidad Facturas que hubo Pendientes " + docs.size());
								LOGGER.info("### Cantidad total de facturas enviadas por JOB : " + cont);
								LOGGER.info("### Facturas enviadas del Emisor: " + infoEmpresa.getRuc() +" - "+infoEmpresa.getRazonSocial());
								
					}
			}
			LOGGER.info("##### Termino de ejecucion del JOB envio de facturas #####");
			
	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException { sendPendingInvoices(); }

	@Autowired
	private InvoicecDocintroService docIntroSrv;
	@Autowired
	private DocumentService documentService;
	@Autowired
	private EmitterService emitterSrv;
	
	private static Logger LOGGER = Logger.getLogger(SendPendingInvoicesJob.class);
}




/**
 * 
 private void processPendingInvoice(InfoEmpresa infoEmp, Document doc) throws SQLException, ParserConfigurationException, SAXException, IOException, Exception {
			// VERIFICAR si documentNumber == legalNumber
			String documentNumber = doc.getNumeroLegal();
			String documentType = doc.getTipoDocumento();
			// TODO ver si esta bien inicializado...
			DocumentProcessCtx documentPK = new DocumentProcessCtx(ruc, documentType, documentNumber);
			String txtFileName = doc.getNombreArchivo();
			String customerMail = "";
			String customerName = "";
			if(doc.getCustomer() != null && doc.getCustomer().getEmail() != null) {customerMail = doc.getCustomer().getEmail();}
			if(doc.getCustomer() != null && doc.getCustomer().getName() != null) {customerName = doc.getCustomer().getName();}
			// TODO usar un pattern
			String pdfGenerated = infoEmp.getDirImpresos() + infoEmp.getRuc() + "-" + documentType + "-" + documentNumber+".PDF";
			//String xmlFileName = HiloHelper.getXmlFileName(documentPK);
			String xmlFileName = documentPK.getXmlFileName();
			SendBillResponse response = null;
			boolean documentAccepted = false;
			try {
					// VERIFICAR si no se puede poner en la config de spring
					//System.setProperty("javax.xml.bind.JAXBContext", "com.sun.xml.internal.bind.v2.ContextFactory");
					System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump","true");
					System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump","true");
					// VERIFICAR si getFileName es equivalente a el fileName del hilo 
					String zipFile = doc.getNombreArchivo().replace(".txt", ".ZIP");
					response = sunatSrv.sendDocument(infoEmp.getDirEnEspera()+zipFile);
					Thread.sleep(2000);
			} catch (Exception e) {
					LOGGER.debug("Error en documento enviado "+ documentNumber +", respuesta SUNAT: " + e.getMessage());
					DBHilo.saveLog(documentPK, "Fallo envio a la SUNAT " + e.getMessage());
					DBHilo.updateDocumentStatus(documentPK, "ER"); 
					//DBHilo.updateDocumentStatus(ruc, documentNumber,"ER", documentType); //transmitido sin respuesta / eror io / timeout
					DBHilo.enviaEmailDocIntro("error", "","","" ,"","",documentNumber, infoEmp.getMailEmpresa(), "");		
					if(doc.getSource()!=null && doc.getSource().equalsIgnoreCase("CN")) { DBHilo.updateConector(doc.getNombreArchivo(), "ER", doc.getDigestValue());	}
			} finally {
					//System.setProperty("javax.xml.bind.JAXBContext", "com.sun.xml.bind.v2.ContextFactory");
					File onHoldFile = new File(infoEmp.getDirEnEspera() + doc.getNombreArchivo().replace(".txt", ".ZIP"));
					onHoldFile.delete();
			}
			if (response != null) {
						DBHilo.saveLog(documentPK, "Se Envio el Documento XML a la Sunat");
						LOGGER.debug("Grabando archivo respuesta en " + infoEmp.getDirFirmados() + "R-" + FilenameUtils.removeExtension(xmlFileName) + ".ZIP");
						String responseZipFileName = infoEmp.getDirFirmados() + "R-" + FilenameUtils.removeExtension(xmlFileName) + ".ZIP";
						File responseFile = new File(responseZipFileName);
						FileUtils.writeByteArrayToFile(responseFile, response.getApplicationResponse());
						LOGGER.debug("Convirtiendo respuesta de documento " + documentNumber + " en UBL Application Response");
						org.w3c.dom.Document responseDoc = DocumentUtil.getDOMFromBytes(DocumentUtil.readFromZip(responseZipFileName));
						ApplicationResponseType applicationResponseDocument = UBL20Reader.readApplicationResponse(responseDoc.getDocumentElement());
						DBHilo.saveLog(documentPK, "Se Recibio Documento XML de Respuesta");
						StringBuffer sbds = new StringBuffer();
						for (DocumentResponseType documentResponse : applicationResponseDocument.getDocumentResponse()) {
								ResponseType resp = documentResponse.getResponse();
								StringBuffer sbar = new StringBuffer();
								for (DescriptionType description : resp.getDescription()) {
											for (NoteType notes : applicationResponseDocument.getNote()) {
												sbar.append(notes.getValue() + String.format("%n"));
											}
											if (resp.getResponseCode().getValue().trim().equals("0")) {
												documentAccepted = true;
											}
											sbds.append("ReferenceID :[ " + resp.getReferenceID().getValue().trim() + "];ResponseCode:["
													+ resp.getResponseCode().getValue().trim() + "];Description:["
													+ description.getValue().trim() + "]" + String.format("%n") + sbar.toString());
								}
						}
	
						DBHilo.saveLog(documentPK, "Leyendo Respuesta");
						LOGGER.info("Exportando respuesta para QAD en " + infoEmp.getDirGenerado() + "out" + File.separator + "R-" + FilenameUtils.removeExtension(xmlFileName) + ".TXT");
						// exportResponse(nameUBLFile, documentType, documentNumber,
						// applicationResponseDocument, "", "1");
						if (documentAccepted) {
							LOGGER.info("Documento " + documentNumber + " Aprobado..");
							LOGGER.info("Moviendo respuesta a " + infoEmp.getDirAutorizados() + "R-" + FilenameUtils.removeExtension(xmlFileName) + ".ZIP");
							File responseZipFile2 = new File(infoEmp.getDirFirmados() + "R-" + FilenameUtils.removeExtension(xmlFileName) + ".ZIP");
							responseZipFile2.renameTo(new File(infoEmp.getDirAutorizados() + "R-" + FilenameUtils.removeExtension(xmlFileName) + ".ZIP"));
							LOGGER.debug("Actualizando documento" + documentNumber + " en base de datos");
							DBHilo.updateDocumentStatus(documentPK, "AT");
							DBHilo.saveLog(documentPK, "Se Autorizo el Documento XML por la SUNAT");
							LOGGER.debug("Envio correo exito " + infoEmp.getDirAutorizados() + FilenameUtils.removeExtension(xmlFileName) + ".PDF");
							DBHilo.enviaEmailDocIntro("exito", "", "",
									infoEmp.getDirAutorizados() + "R-" + FilenameUtils.removeExtension(xmlFileName) + ".ZIP",
									pdfGenerated, customerMail, documentNumber, infoEmp.getMailEmpresa(), customerName);
							if(doc.getSource()!=null && doc.getSource().equalsIgnoreCase("CN")) { DBHilo.updateConector(doc.getNombreArchivo(), "AT", doc.getDigestValue());	}
							
							
							
						} else {
							File responseZipFile2 = new File(infoEmp.getDirFirmados() + "R-" + FilenameUtils.removeExtension(xmlFileName) + ".ZIP");
							responseZipFile2.renameTo(new File(infoEmp.getDirNoAutorizados() + "R-"	+ FilenameUtils.removeExtension(xmlFileName) + ".ZIP"));
							// saveLog(documentNumber,"RS","","Documento no autorizado","","","","");
							DBHilo.updateDocumentStatus(documentPK, "RH");
							DBHilo.saveLog(documentPK, "Se Rechazo el Documento XML por la SUNAT");
							// enviaEmailCliente("error", emite, "","","" ,"", (String)
							// hInputHeader.get("QADEmailCreador"),documentNumber);
							DBHilo.enviaEmailDocIntro("error", "", "",
									infoEmp.getDirNoAutorizados() + "R-" + FilenameUtils.removeExtension(xmlFileName) + ".ZIP",
									pdfGenerated, "", documentNumber, infoEmp.getMailEmpresa(), "");							
							if(doc.getSource()!=null && doc.getSource().equalsIgnoreCase("CN")) { DBHilo.updateConector(doc.getNombreArchivo(), "RH", doc.getDigestValue());	}
						}
	
			}//FIN DE IF RESPONSE ES  NULL
			LOGGER.info("Fin procesamiento archivo  " + txtFileName);
	} 
 * 
 * 
 * 
 */


/**
public void sendPendingInvoices2() {
	// TODO hardcodeado
	String documentType = "01";
	String documentNumber = "0";
	// TODO revisar
	DocumentProcessCtx documentPK = new DocumentProcessCtx(ruc, documentType, documentNumber);
	// TODO mover a la conf. de spring...
	if (!docIntroSrv.existeEmpresa(ruc)) {
		LOGGER.error("La empresa no se encuentra registrada");
		return;
	}
	
	InfoEmpresa infoEmpresa = docIntroSrv.obtieneInfoEmpresa(ruc);
	String ruta = infoEmpresa.getDirFirmados();
	Emisor emisor;
	for (File zipFile : buscarExistentes(ruta, ".zip")) {
		try {
			System.setProperty("javax.xml.bind.JAXBContext", "com.sun.xml.internal.bind.v2.ContextFactory");
			System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump","true");
			System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump","true");
			sunatSrv.sendDocument(zipFile.getAbsolutePath());
		} catch (Exception e) {
			LOGGER.debug("Error en documento enviado "+ documentNumber +", respuesta SUNAT: " + e.getMessage());
			DBHilo.saveLog(documentPK, "Fallo envio a la SUNAT " + e.getMessage());
			//DBHilo.updateDocumentStatus(ruc, documentNumber,"ER", documentType); //transmitido sin respuesta / eror io / timeout
			DBHilo.enviaEmailCliente("error", "","","" ,"","",documentNumber, infoEmpresa.getMailEmpresa(), "");									
		} finally {
			System.setProperty("javax.xml.bind.JAXBContext", "com.sun.xml.bind.v2.ContextFactory");
		}
	}
	LOGGER.info("iniciando job con ruc "+ruc);
}**/
