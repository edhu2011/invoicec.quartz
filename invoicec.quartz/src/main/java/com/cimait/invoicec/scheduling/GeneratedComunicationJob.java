package com.cimait.invoicec.scheduling;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.cimait.invoicec.entity.Document;
import com.cimait.invoicec.entity.Emitter;
import com.cimait.invoicec.service.InvoicecDocintroService;
import com.cimait.invoicec.service.InvoicecProcessBackgroundService;
import com.cimait.invoicec.service.repository.DocumentService;
import com.cimait.invoicec.service.repository.EmitterService;
import com.cimait.invoicec.service.repository.SummaryBajaServices;
import com.cimait.invoicec.util.InfoEmpresa;


@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class GeneratedComunicationJob extends QuartzJobBean
{
	
	@Override
	protected void executeInternal(JobExecutionContext context)	throws JobExecutionException { generateComunicacionBaja();}

	public void generateComunicacionBaja(){
		try {
				LOGGER.info("### Ejecutandose JOB de Generacion de Comunicacion de Baja");
				LOGGER.info("Hora de Ejecucion: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a").format(new Date()));
				List<Emitter> emitters = emitterSrv.findAllEmiters();
				
				for (Emitter emitter : emitters) {
				
					InfoEmpresa infoEmpresa = docIntroSrv.obtieneInfoEmpresa(emitter.getIdentification());					
														
					/**OBTENEMOS LISTA DE DOCS DADOS DE BAJA SEGUN RUC EMISOR, ORDENADOS POR FECHA EMISION**/					
					List<Document> lstFechaEmision = documentService.obtenerFechaEmisionDeDocsPB(infoEmpresa.getRuc());					
										
					int correlativo = summaryBajaServices.getCorrelativoSummary(infoEmpresa.getRuc(), "RA");
					for (Document doc : lstFechaEmision) {	
						
								/**OBTENEMOS LISTA DOCS SEGUN FECHA DE EMISION **/
								List<Document> documentosBaja = documentService.docPendientesDeBaja(doc.getFechaEmision(), doc.getRuc());
								
								List<Document> lstDocs = new ArrayList<Document>();
								Date issueDate = null;
								++correlativo;
								for (Document documento : documentosBaja) {
									
									Document d = new Document();
									issueDate = documento.getFechaEmision();
									d.setNumeroLegal(documento.getNumeroLegal());
									d.setTipoDocumento(documento.getTipoDocumento());
									d.setFechaEmision(documento.getFechaEmision());
									lstDocs.add(d);
								}
									if(lstDocs.size() > 0){
									
										docIntroSrv.generarComunicacionBaja(lstDocs, issueDate, String.format("%05d",correlativo), new Date(), infoEmpresa);
										procesaResumen.procesa(infoEmpresa);
									}
								
					}
				}			
			
		} catch (Exception e)
			{
				LOGGER.error("Ocurrio un error en la ejecucion del JOB de Generacion de Resumen de Baja de Facturas. \n" +e);
				e.printStackTrace();
			
			}
	}
	
	
	
	
	
	
	
	
	public void generateComunicacionBaja(InfoEmpresa infoEmpresa){
		try {
			LOGGER.info("### Generando Comunicacion de Baja");
			LOGGER.info("Hora de Ejecucion: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a").format(new Date()));
																					
			/***OBTENEMOS LISTA DE DOCS DADOS DE BAJA SEGUN RUC EMISOR, ORDENADOS POR FECHA EMISION***/
			List<Document> lstFechaEmision = documentService.obtenerFechaEmisionDeDocsPB(infoEmpresa.getRuc());					
									
			int correlativo = summaryBajaServices.getCorrelativoSummary(infoEmpresa.getRuc(), "RA");
			for (Document doc : lstFechaEmision) {
					
				/**OBTENEMOS LISTA DOCS SEGUN FECHA DE EMISION***/ 
				List<Document> documentosBaja = documentService.docPendientesDeBaja(doc.getFechaEmision(), doc.getRuc());
							
				List<Document> lstDocs = new ArrayList<Document>();
				Date issueDate = null;
				++correlativo;
				for (Document documento : documentosBaja) {
					Document d = new Document();
					issueDate = documento.getFechaEmision();
					d.setNumeroLegal(documento.getNumeroLegal());
					d.setTipoDocumento(documento.getTipoDocumento());
					d.setFechaEmision(documento.getFechaEmision());
					lstDocs.add(d);
				}
				if(lstDocs.size() > 0){
					docIntroSrv.generarComunicacionBaja(lstDocs, issueDate, String.format("%05d",correlativo), new Date(), infoEmpresa);
					procesaResumen.procesa(infoEmpresa);
				}
			}
						
		
		}catch (Exception e)
		{
				LOGGER.error("Ocurrio un error en la ejecucion del JOB de Generacion de Resumen de Baja de Facturas. \n" +e);
				e.printStackTrace();			
		}
	}
	
	
	@Autowired
	private SummaryBajaServices summaryBajaServices;
	@Autowired
	private InvoicecDocintroService docIntroSrv;
	@Autowired
	private DocumentService documentService;
	@Autowired
	private EmitterService emitterSrv;
	@Autowired
	private InvoicecProcessBackgroundService procesaResumen;
	private static Logger LOGGER = Logger.getLogger(GeneratedComunicationJob.class);

}
