package com.cimait.invoicec.scheduling;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.cimait.invoicec.entity.Document;
import com.cimait.invoicec.entity.Emitter;
import com.cimait.invoicec.service.InvoicecDocintroService;
import com.cimait.invoicec.service.InvoicecProcessBackgroundService;
import com.cimait.invoicec.service.repository.DocumentService;
import com.cimait.invoicec.service.repository.EmitterService;
import com.cimait.invoicec.service.repository.ReceiptSummaryService;
import com.cimait.invoicec.util.DateUtil;
import com.cimait.invoicec.util.InfoEmpresa;



@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class SendReceiptSummaryJob extends QuartzJobBean{
	
	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException { 		
		generateReceiptSummaryMulti();
	}


	public void generateReceiptSummaryMulti() {
		try {
								
				LOGGER.info("### Ejecutandose JOB de Generacion de Resumen de Boleta");
				LOGGER.info("Hora de Ejecucion: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a").format(new Date()));
				
				List<Emitter> emitters = emitterSrv.findAllEmiters();
				for (Emitter emitter : emitters) {
					InfoEmpresa infoEmpresa = docIntroSrv.obtieneInfoEmpresa(emitter.getIdentification());					
					CANTIDAD_MAX_LINEAS = infoEmpresa.getCantidadMax() != null && infoEmpresa.getCantidadMax().matches("[0-9]*") ? Integer.parseInt(infoEmpresa.getCantidadMax()) : CANTIDAD_MAX_LINEAS;
					
					LOGGER.info("Buscando boletas de la empresa: "+infoEmpresa.getRazonSocial());
					
					/**OBTENEMOS LISTA DE DOCS DADOS DE BOLETAS SEGUN RUC EMISOR, ORDENADOS POR FECHA EMISION**/					
					List<Document> lstFechaEmision = documentService.obtenerFechaEmisionDeDocsPD(infoEmpresa.getRuc(), infoEmpresa.getDiasAtrasBoleta());
					//LOGGER.info("Total de fechas en boletas PD: "+lstFechaEmision.size());
					
					for (Document doc : lstFechaEmision) {
						LOGGER.info("Buscando boletas PD con fecha: "+doc.getFechaEmision());
						
						/**OBTENEMOS LISTA DOCS SEGUN FECHA DE EMISION **/
						List<Document> boletas = documentService.boletasPendientesNoEnviadas(doc.getFechaEmision(), infoEmpresa.getRuc());
						LOGGER.info("Total de boletas PD encontradas: "+boletas.size());
						if(boletas.size() > 0 && boletas.size() <= CANTIDAD_MAX_LINEAS){					
							int correlativo = receiptSummaryService.getCorrelativoSummary(infoEmpresa.getRuc());
							LOGGER.info("Generando resumen: " +infoEmpresa.getRuc() + "-RC-" + DateUtil.formatDateSummaryFileName(new Date()) +  "-" + String.format("%05d",correlativo + 1) + ".txt");
							//proceso 01: generar los resumenes
							docIntroSrv.generarResumen(boletas, boletas.get(0).getFechaEmision(), String.format("%05d",correlativo + 1), new Date(), infoEmpresa);
							
							 //proceso 02: procesar cada resumen generado
							procesaResumen.procesa(infoEmpresa);
							
						}
						else{	
							if(boletas.size() > CANTIDAD_MAX_LINEAS){
								int TOTAL_BOLETAS_PENDIENTES = boletas.size();
								int correlativo = receiptSummaryService.getCorrelativoSummary(infoEmpresa.getRuc());
							    for (int i = 0; i < TOTAL_BOLETAS_PENDIENTES; i+=CANTIDAD_MAX_LINEAS) {
							    	correlativo++;						    	
							    	List<Document> subListBoletas = boletas.subList(i, Math.min(TOTAL_BOLETAS_PENDIENTES, i + CANTIDAD_MAX_LINEAS));
							    	LOGGER.info("Generando resumen: " +infoEmpresa.getRuc() + "-RC-" + DateUtil.formatDateSummaryFileName(new Date()) +  "-" + String.format("%05d",correlativo) + ".txt");
							    	//proceso 01: generar los resumenes
									docIntroSrv.generarResumen(subListBoletas, subListBoletas.get(0).getFechaEmision(), String.format("%05d",correlativo), new Date(), infoEmpresa);
									
								}
							    					    
							    //proceso 02: procesar cada resumen generado					  
							    procesaResumen.procesa(infoEmpresa);
							    Thread.sleep(35000);
						    
							}	
						}
						
						
						
					}

					
				}
	
		} catch (Exception e){
				LOGGER.error("Ocurrio un error en la ejecucion del JOB de Generacion de Resumen de Boleta. \n" +e);
				e.printStackTrace();			
		}
		
	}
	
	
	public void generateReceiptSummary(Date fechaEmisionDocumento, InfoEmpresa infoEmpresa){
		try {
				String diasAtras = DateUtil.obtenerCantidadDiasAtras(fechaEmisionDocumento);
				List<Document> boletas = documentService.boletasPendientesQuartzMulti(diasAtras, infoEmpresa.getRuc());
				
				if(boletas.size() > 0 && boletas.size() <= CANTIDAD_MAX_LINEAS){					
					int correlativo = receiptSummaryService.getCorrelativoSummary(infoEmpresa.getRuc());
					LOGGER.info("### Generando resumen: " +infoEmpresa.getRuc() + "-RC-" + DateUtil.formatDateSummaryFileName(new Date()) +  "-" + String.format("%05d",correlativo + 1) + ".txt");
					LOGGER.info("### Cantidad boletas "+ boletas.size() + " boletas.");
					LOGGER.info("### Ruc Emisor " + infoEmpresa.getRuc());
					LOGGER.info("### Name Emisor " + infoEmpresa.getRazonSocial());
					docIntroSrv.generarResumen(boletas, boletas.get(0).getFechaEmision(), String.format("%05d",correlativo + 1), new Date(), infoEmpresa);
					
					procesaResumen.procesa(infoEmpresa);
					
				}
				else{	
					if(boletas.size() > CANTIDAD_MAX_LINEAS){
						int TOTAL_BOLETAS_PENDIENTES = boletas.size();
						int correlativo = receiptSummaryService.getCorrelativoSummary(infoEmpresa.getRuc());
					    for (int i = 0; i < TOTAL_BOLETAS_PENDIENTES; i+=CANTIDAD_MAX_LINEAS) {
					    	correlativo++;						    	
					    	List<Document> subListBoletas = boletas.subList(i, Math.min(TOTAL_BOLETAS_PENDIENTES, i + CANTIDAD_MAX_LINEAS));
					    	LOGGER.info("### Generando resumen: " +infoEmpresa.getRuc() + "-RC-" + DateUtil.formatDateSummaryFileName(new Date()) +  "-" + String.format("%05d",correlativo) + ".txt");
					    	LOGGER.info("### Cantidad boletas "+ subListBoletas.size() + " boletas.");						    	
							LOGGER.info("### Ruc Emisor " + infoEmpresa.getRuc());
							LOGGER.info("### Name Emisor " + infoEmpresa.getRazonSocial());
							docIntroSrv.generarResumen(subListBoletas, subListBoletas.get(0).getFechaEmision(), String.format("%05d",correlativo), new Date(), infoEmpresa);
						}
					    
					    procesaResumen.procesa(infoEmpresa);
					    Thread.sleep(35000);
					    
					}	
				}

		} catch (Exception e) {
			LOGGER.error("Ocurrio un error en la ejecucion del JOB de Generacion de Resumen de Boleta. \n" +e);
			e.printStackTrace();			
		}
	}
	
	private int CANTIDAD_MAX_LINEAS = 1000;	
	@Autowired
	private InvoicecDocintroService docIntroSrv;
	@Autowired
	private DocumentService documentService;
	@Autowired
	private EmitterService emitterSrv;
	@Autowired
	private ReceiptSummaryService receiptSummaryService;
	@Autowired
	private InvoicecProcessBackgroundService procesaResumen;
	
	private static Logger LOGGER = Logger.getLogger(SendReceiptSummaryJob.class);	
	
}


/**
public void generateReceiptSummaryMulti_V1() {
	try {
			
			LOGGER.info("### Ejecutandose JOB de Generacion de Resumen de Boleta");
			LOGGER.info("Hora de Ejecucion: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a").format(new Date()));
			List<Emitter> emitters = emitterSrv.findAllEmiters();
			for (Emitter emitter : emitters) {
				InfoEmpresa infoEmpresa = docIntroSrv.obtieneInfoEmpresa(emitter.getIdentification());
				List<Document> boletas = documentService.boletasPendientesQuartzMulti(infoEmpresa.getDiasAtrasBoleta(), infoEmpresa.getRuc());
									
				if(boletas.size() > 0 && boletas.size() <= CANTIDAD_MAX_LINEAS){					
					int correlativo = receiptSummaryService.getCorrelativoSummary(infoEmpresa.getRuc());
					LOGGER.info("### Generando resumen: " +infoEmpresa.getRuc() + "-RC-" + DateUtil.formatDateSummaryFileName(new Date()) +  "-" + String.format("%05d",correlativo + 1) + ".txt");
					LOGGER.info("### Cantidad boletas "+ boletas.size() + " boletas.");
					LOGGER.info("### Ruc Emisor " + infoEmpresa.getRuc());
					LOGGER.info("### Name Emisor " + infoEmpresa.getRazonSocial());
					//proceso 01: generar los resumenes
					docIntroSrv.generarResumen(boletas, boletas.get(0).getFechaEmision(), String.format("%05d",correlativo + 1), new Date(), infoEmpresa);
					
					 //proceso 02: procesar cada resumen generado
					procesaResumen.procesa(infoEmpresa);
					
				}
				else{	
					if(boletas.size() > CANTIDAD_MAX_LINEAS){
						int TOTAL_BOLETAS_PENDIENTES = boletas.size();
						int correlativo = receiptSummaryService.getCorrelativoSummary(infoEmpresa.getRuc());
					    for (int i = 0; i < TOTAL_BOLETAS_PENDIENTES; i+=CANTIDAD_MAX_LINEAS) {
					    	correlativo++;						    	
					    	List<Document> subListBoletas = boletas.subList(i, Math.min(TOTAL_BOLETAS_PENDIENTES, i + CANTIDAD_MAX_LINEAS));
					    	LOGGER.info("### Generando resumen: " +infoEmpresa.getRuc() + "-RC-" + DateUtil.formatDateSummaryFileName(new Date()) +  "-" + String.format("%05d",correlativo) + ".txt");
					    	LOGGER.info("### Cantidad boletas "+ subListBoletas.size() + " boletas.");						    	
							LOGGER.info("### Ruc Emisor " + infoEmpresa.getRuc());
							LOGGER.info("### Name Emisor " + infoEmpresa.getRazonSocial());
					    	//proceso 01: generar los resumenes
							docIntroSrv.generarResumen(subListBoletas, subListBoletas.get(0).getFechaEmision(), String.format("%05d",correlativo), new Date(), infoEmpresa);
							
						}
					    					    
					    //proceso 02: procesar cada resumen generado					  
					    procesaResumen.procesa(infoEmpresa);
					    Thread.sleep(35000);
				    
					}	
				}
				
			}

	} catch (Exception e){
			LOGGER.error("Ocurrio un error en la ejecucion del JOB de Generacion de Resumen de Boleta. \n" +e);
			e.printStackTrace();			
	}
	
}	
**/