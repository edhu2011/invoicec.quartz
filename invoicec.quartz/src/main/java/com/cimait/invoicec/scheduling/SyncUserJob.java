package com.cimait.invoicec.scheduling;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cimait.invoicec.entity.Emitter;
import com.cimait.invoicec.entity.User;
import com.cimait.invoicec.entity.UserRole;
import com.cimait.invoicec.repository.EmitterRepository;
import com.cimait.invoicec.service.InvoicecDocintroService;
import com.cimait.invoicec.service.repository.EmitterService;
import com.cimait.invoicec.service.repository.UserRoleService;
import com.cimait.invoicec.service.repository.UserService;
import com.cimait.invoicec.util.FileUtil;
import com.cimait.invoicec.util.InfoEmpresa;
import com.cimait.invoicec.util.SFTPDownloader;
import com.jcraft.jsch.SftpException;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class SyncUserJob extends QuartzJobBean {
	static List<User> lstuser = null;
	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		try {			
			infoEmpresa = docIntroSrv.getInfoEmpresaForUserJobEBIZ();
			leerTxtUser();
		} catch (Exception e) {
			LOGGER.error(" Error al leer el TXT\n ",e);
			e.printStackTrace();
		}
	}

	
/**METODO PARA LA LECTURA DESDE UN SFTP***/

public void leerTxtUser() throws SftpException {
	
	//List<Emitter> emitter = emitterService.findAllByOneCompany();
	LOGGER.info("### Ejecutandose JOB de SyncUserJob.");
	LOGGER.info("### Hora de Ejecucion: " + new Date());

	downloadFileUserSftp();
	archivosUsers = FileUtil.busqueda(infoEmpresa.getRecibidosSftpFtpUsuarios(), ".txt");
	//archivosUsers = FileUtil.busqueda("D:/cimait/invoice/repository/ebiz/ftprecibidospusers/", ".txt");
	
		
	int countFiles = 0;
	try {
		if (archivosUsers != null && archivosUsers.length > 0) {
			for (int i = 0; i < archivosUsers.length; i++) {
				String file = archivosUsers[i].getAbsolutePath();
				boolean existe = archivosUsers[i].exists();
				if (existe) {
					FileReader reader = new FileReader(file);
					BufferedReader lectura= new BufferedReader(reader);
					//reader = new CSVReader(new FileReader(file), '|');
					String linea;
					lstuser = new ArrayList<User>();
					LOGGER.info("    Inicio de Lectura de Usuarios.");
					countFiles = i;
					while ((linea = lectura.readLine()) != null) {
						//System.out.println("Linea  numero: " + contador);
						User user = new User();
						String nextLine[] = linea.split("\\|");
						
						if(nextLine[7].equalsIgnoreCase("E")){
							
							/** VALIDAR SI EXISTE EL EMISOR EN LA BD **/
							
							user.setRuc(nextLine[0].trim());
							user.setRucEmpresa("");
							user.setTipoUsuario(nextLine[7] + nextLine[8]);/** Si campo7 es E-> Emisor **/
						}
						if(nextLine[7].equalsIgnoreCase("R")){
							user.setRuc(esRuc(nextLine[0].trim()) ? nextLine[0].trim() : "99999999999");
							user.setRucEmpresa(esRuc(nextLine[0].trim()) ? nextLine[0].trim() : "99999999999");
							user.setTipoUsuario(nextLine[7] + nextLine[8]);/** Si campo7 es R-> Receptor **/
						}																					
						
						String nombre = clearCadena(nextLine[3].trim()) + " " + clearCadena(nextLine[4].trim());
						user.setCodUsuario(nextLine[1].trim().length() > 20 ? nextLine[1].trim().substring(0, 20) : nextLine[1].trim()); 
						user.setPassword(nextLine[2].trim());
						user.setNombre(clearCadena(nombre.trim()).length() > 100 ? clearCadena(nombre.trim()).substring(0, 100) : clearCadena(nombre.trim()));

						if(nextLine[5].trim().equalsIgnoreCase("1")){user.setIsActive("Y");} else {user.setIsActive("N");}
						user.setEmail("");
						
						if(getRolUser(user.getTipoUsuario()) != null){
							lstuser.add(user);
						}						
						//LOGGER.info("    Lectura correcta del usuario ["+contador+"]: "+(nextLine[1].trim().length() > 20 ? nextLine[1].trim().substring(0, 20) : nextLine[1].trim()));
						
					}
						Thread.sleep(2000);
						LOGGER.info("### Se encontraron: "+ lstuser.size() + " usuarios en el archivo: "+archivosUsers[i].getName());
						Thread.sleep(2000);
						guardarUsuarios(lstuser);
						reader.close();
						FileUtil.moverArchivo(infoEmpresa.getRecibidosUsuarios(),archivosUsers[i]);
						LOGGER.info("Moviendo archivo: " + archivosUsers[i] + " al directorio " + infoEmpresa.getRecibidosUsuarios());
						lstuser.clear();
						Thread.sleep(2000);
				}else{ LOGGER.error("El sistema no puede encontrar el archivo especificado: "+file); }
			}
		}
		

	} catch (Exception e) {
		LOGGER.error("No se pudo concretar la lectura de archivos.... \n" , e);
		LOGGER.error("Archivo .... " + archivosUsers[countFiles].getName());
		e.printStackTrace();
	}
	
}


public void downloadFileUserSftp() throws SftpException{
	
	SFTPDownloader sftp = new SFTPDownloader(infoEmpresa.getFtpHost(), infoEmpresa.getFtpUser(), infoEmpresa.getFtpPassword(), infoEmpresa.getFtpDirectory(), infoEmpresa.getFtpExtension(),Integer.parseInt(infoEmpresa.getFtpPort()));
	sftp.connect();
	sftp.downloadFileUser(infoEmpresa.getRecibidosSftpFtpUsuarios(), infoEmpresa.getRecibidosUsuarios(), infoEmpresa.getFtpDirectory(), infoEmpresa.getEbizBackup());
	sftp.disconnect();
	
}

@Transactional
public void guardarUsuarios(List<User> listaUser) throws InterruptedException {
		int linea = 0;
		int existe=0;
		int nuevo=0;
		User usuario = null;
		UserRole usuarioRole = null;
		for (User user : listaUser) {
				linea++;				
				LOGGER.info("    Guardando en la BD al usuario ["+linea+"]: "+ user.getCodUsuario());
				//usuario = userService.findByRucAndCodUsuario(user.getRuc(), user.getCodUsuario());
				
				//TODO solo valida si existe "CodUsuario" este campo debe ser unico.
				usuario = userService.findByCodUsuario(user.getCodUsuario());
				
				if(usuario != null)
				{
						existe++;						
						/*usuario.setNombre(user.getNombre());*/
						usuario.setNombre(clearCadena(user.getNombre()));
						usuario.setPassword(user.getPassword());
						usuario.setIsActive(user.getIsActive());
						usuario.setTipoUsuario(user.getTipoUsuario().substring(0,1));						
						userService.save(usuario);
						
						
						//usuarioRole = userRoleService.findByCodUsuario(user.getCodUsuario());
						
						//TODO solo valida si existe "CodUsuario" este campo debe ser unico.
						usuarioRole = userRoleService.findByCodUsuario(user.getCodUsuario());
						if(usuarioRole != null) userRoleService.delete(usuarioRole);
						Thread.sleep(3000);
						usuarioRole = new UserRole();
						
						String codRol = getRolUser(user.getTipoUsuario());
						usuarioRole.setRuc(user.getRuc());
						usuarioRole.setCodUsuario(user.getCodUsuario());
						usuarioRole.setCodRol(codRol);
						usuarioRole.setIsActive(user.getIsActive());
						userRoleService.save(usuarioRole);
						usuario = null;
						usuarioRole = null;
				}else{
						nuevo++;
						User newUser = new User();
						
						newUser.setRuc(user.getRuc());
						newUser.setRucEmpresa(user.getRucEmpresa());
						
						newUser.setCodUsuario(user.getCodUsuario());
						newUser.setPassword(user.getPassword());
						newUser.setNombre(clearCadena(user.getNombre()));
						newUser.setTipoUsuario(user.getTipoUsuario().substring(0,1));
						newUser.setIsActive(user.getIsActive());
						newUser.setEmail(user.getEmail());
						
						UserRole userRole = new UserRole();
						userRole.setRuc(user.getRuc());
						userRole.setCodUsuario(user.getCodUsuario());
						
						String codRol = getRolUser(user.getTipoUsuario());
						userRole.setCodRol(codRol);
						
						userRole.setIsActive(user.getIsActive());
						userService.save(newUser);				
						userRoleService.save(userRole);
						usuario = null;
						usuarioRole = null;
				}			
							
		}
		LOGGER.info("CANTIDAD DE USUARIOS NUEVOS: "+ nuevo);
		LOGGER.info("CANTIDAD DE USUARIOS EXISTENTES: "+ existe);
		
	}
	/* Validacion: Mitigar error cuando el tipoUsuario no coincide con ni una Condicion  */
	private String getRolUser(String tipoUsuario){
		String codRol = null;
		if(tipoUsuario != null){
			if(tipoUsuario.equalsIgnoreCase("EF")) { codRol = "Ventas";    }
			if(tipoUsuario.equalsIgnoreCase("ER")) { codRol = "Compras";   }
			if(tipoUsuario.equalsIgnoreCase("EA")) { codRol = "Admin";     }			
			if(tipoUsuario.equalsIgnoreCase("RF")) { codRol = "Cliente";   }
			if(tipoUsuario.equalsIgnoreCase("RR")) { codRol = "Proveedor"; }
		}
						
		return codRol;
	}

	private boolean getTipeUser(String ruc){
		boolean tipUser=false;		
		Emitter emitter = emitterRepository.findOneByIdentification(ruc);
		if (emitter!= null){
			tipUser=true;
		}		
		return tipUser;
	}
	
	 private boolean esRuc(String valor) {  
		  String expReg = "^[0-9]{11}$";
		        return valor.matches(expReg);
	 }
	 
	 private String clearCadena(String cadena){

		 	String reemplazado = "";
			String expReg = "[0-9+|\\-+|\\_+|\\:+|\\.+|\\�+|\\?+|\\�+|\\�+|\\|+|\\!+|\\#+|\\$+|" +
					  "\\%+|\\&+|\\+|\\=+|\\�+|\\�+|\\++|\\*+|\\~+|\\[+|\\]" +
					  "+|\\{+|\\}+|\\^+|\\<+|\\>+|\\\"+]";				
			reemplazado = cadena.replaceAll(expReg, "").trim();
			return reemplazado;
				
		}

	
	@Autowired
	protected EmitterRepository emitterRepository;
	@Autowired
	private   EmitterService emitterService;
	@Autowired
	private   InvoicecDocintroService docIntroSrv;
	@Autowired
	private   UserService userService;
	@Autowired
	private UserRoleService userRoleService;		
	private File[] archivosUsers;	
	private InfoEmpresa infoEmpresa = null;
	
	private Logger LOGGER = Logger.getLogger(SyncUserJob.class);
	

}


/**
public void downloadFileUserSftp(Emitter emitter) throws SftpException{	
	
	//SFTPDownloader sftp = new SFTPDownloader(emitter.getFtpHost(), emitter.getFtpUser(), emitter.getFtpPassword(), emitter.getFtpDirectory(), emitter.getFtpExtension(),Integer.parseInt(sftpPort.trim()));
	SFTPDownloader sftp = new SFTPDownloader(host, user, password, pathRaiz, ext,Integer.parseInt(sftpPort.trim()));
	sftp.connect();										
	//sftp.downloadFileUser(ftpBajadosUser, emitter.getPathReception(), emitter.getFtpDirectory(), directoryBackupRemote );
	sftp.downloadFileUser(ftpBajadosUser, pathreception, pathRaiz, directoryBackupRemote );
	sftp.disconnect();
	
}**/
/***
public void leerTxtUser() throws SftpException {
	
	List<Emitter> emitter = emitterService.findAllByOneCompany();
	LOGGER.info("### Ejecutandose JOB de SyncUserJob.");
	LOGGER.info("### Hora de Ejecucion: " + new Date());
	downloadFileUserSftp(emitter.get(0));
	archivosUsers = FileUtil.busqueda(ftpBajadosUser, ".txt");
	
	try {
		if (archivosUsers != null && archivosUsers.length > 0) {
			for (int i = 0; i < archivosUsers.length; i++) {
				String file = archivosUsers[i].getAbsolutePath();
				boolean existe = archivosUsers[i].exists();
				if (existe) {
					reader = new CSVReader(new FileReader(file), '|');
					String[] nextLine;
					lstuser = new ArrayList<User>();
					LOGGER.info("    Inicio de Lectura de Usuarios.");
					int contador = 0;
					while ((nextLine = reader.readNext()) != null) {
						contador++;
						User user = new User();
						
						if(getTipeUser(nextLine[0].trim())){
							user.setRuc(nextLine[0].trim());
							user.setRucEmpresa("");
							user.setTipoUsuario("E");//SI EXISTE RUC EN LA BD ENTONCES EL Tip.Usu SERA E
							
						}else{
							//user.setRuc(emitterService.findAllByOrdeCompany());  //esRuc(nextLine[0].trim()) ? nextLine[0].trim() : "20502007018"
							//user.setRucEmpresa(nextLine[0].trim());
							user.setRuc(esRuc(nextLine[0].trim()) ? nextLine[0].trim() : "99999999999");  //esRuc(nextLine[0].trim()) ? nextLine[0].trim() : "20502007018"
							user.setRucEmpresa(esRuc(nextLine[0].trim()) ? nextLine[0].trim() : "99999999999");
							user.setTipoUsuario("C");//SI NO EXISTE RUC EN LA BD ENTONCES EL Tip.Usu C
						}
						
						String nombre = clearCadena(nextLine[3].trim()) + " " + clearCadena(nextLine[4].trim());
						user.setCodUsuario(nextLine[1].trim().length() > 20 ? nextLine[1].trim().substring(0, 20) : nextLine[1].trim()); 
						user.setPassword(nextLine[2].trim());
						user.setNombre(clearCadena(nombre.trim()).length() > 100 ? clearCadena(nombre.trim()).substring(0, 100) : clearCadena(nombre.trim()));
						
						if(nextLine[5].trim().equalsIgnoreCase("1")){user.setIsActive("Y");} else {user.setIsActive("N");}
						user.setEmail("");
						lstuser.add(user);
						//LOGGER.info("    Lectura correcta del usuario ["+contador+"]: "+(nextLine[1].trim().length() > 20 ? nextLine[1].trim().substring(0, 20) : nextLine[1].trim()));
						
					}
						Thread.sleep(2000);
						LOGGER.info("### Se encontraron: "+ lstuser.size() + " usuarios en el archivo: "+archivosUsers[i].getName());
						Thread.sleep(2000);
						guardarUsuarios(lstuser);
						reader.close();
						FileUtil.moverArchivo(emitter.get(0).getPathReception(),archivosUsers[i]);
						lstuser.clear();
						Thread.sleep(2000);
				}else{ LOGGER.error("El sistema no puede encontrar el archivo especificado: "+file); }
			}
		}
		

	} catch (Exception e) {
		e.printStackTrace();
	}
	
}	*****/
/**METODO PARA LA LECTURA DESDE UN SFTP

public void leerTxtUser() throws SftpException {
	
	infoEmpresa = docIntroSrv.obtieneInfoEmpresa(ruc);
	LOGGER.info("### Ejecutandose JOB de SyncUserJob.");
	LOGGER.info("### Hora de Ejecucion: " + new Date());
	downloadFileUserFtp();
	archivosUsers = FileUtil.busqueda(infoEmpresa.getPathFtpRecibido(), ".xml", ".txt");
	
	try {
		if (archivosUsers != null && archivosUsers.length > 0) {
			for (int i = 0; i < archivosUsers.length; i++) {
				String file = archivosUsers[i].getAbsolutePath();
				boolean existe = archivosUsers[i].exists();
				if (existe) {
					reader = new CSVReader(new FileReader(file), '|');
					String[] nextLine;
					List<User> lstuser = new ArrayList<User>();
					while ((nextLine = reader.readNext()) != null) {
						
						User user = new User();
						user.setRuc(nextLine[0].trim());
						user.setCodUsuario(nextLine[1].trim());
						user.setPassword(nextLine[2].trim());
						user.setNombre(nextLine[3].trim());
						user.setTipoUsuario("E");
						user.setIsActive("Y");
						user.setEmail("");
						lstuser.add(user);
	
					}
						LOGGER.info("### Se encontraron: "+ lstuser.size() + " usuarios en el archivo: "+archivosUsers[i].getName());
						guardarUsuarios(lstuser);
						reader.close();						
						moverArchivo(archivosUsers[i]);
				}else{ LOGGER.error("El sistema no puede encontrar el archivo especificado: "+file); }
			}
		}
		

	} catch (Exception e) {
		e.printStackTrace();
	}
	
}



public void downloadFileUserSftp() throws SftpException{	
	
	InfoEmpresa infoEmpresa = docIntroSrv.obtieneInfoEmpresa(ruc);
	String path = infoEmpresa.getPathFtpRecibido();	
	SFTPDownloader sftp = new SFTPDownloader("181.65.131.71", "ebizsftp", "SftpQas3b1z", "/C/Users/ebizsftp", "txt");
	//SFTPDownloader sftp = new SFTPDownloader("198.199.65.58", "boris", "123456789", "/incoming", "txt");
	sftp.connect();									
	sftp.downloadFileUser(path);
	sftp.disconnect();
	
}***/


/**METODO PARA LA LECTURA DESDE UN FTP**/
/**
public void leerTxtUser() throws SftpException {
	String estado="N";
	String tipU="C";
	List<Emitter> emitter = emitterService.findAllByOneCompany();
	LOGGER.info("### Ejecutandose JOB de SyncUserJob.");
	LOGGER.info("### Hora de Ejecucion: " + new Date());
	downloadFileUserFtp(emitter.get(0));
	archivosUsers = FileUtil.busqueda(emitter.get(0).getPathGenerated(),".txt");
	
	try {
		if (archivosUsers != null && archivosUsers.length > 0) {
			for (int i = 0; i < archivosUsers.length; i++) {
				String file = archivosUsers[i].getAbsolutePath();
				boolean existe = archivosUsers[i].exists();
				if (existe) {
					reader = new CSVReader(new FileReader(file), '|');
					String[] nextLine;
					List<User> lstuser = new ArrayList<User>();
					while ((nextLine = reader.readNext()) != null){
						User user = new User();
						
						if(getTipeUser(nextLine[0].trim())){
							user.setRuc(nextLine[0].trim());
							user.setRucEmpresa("");
							tipU="E";
						}else{
							user.setRuc(emitterService.findAllByOrdeCompany());
							user.setRucEmpresa(nextLine[0].trim());
						}
						
						user.setCodUsuario(nextLine[1].trim());
						user.setPassword(nextLine[2].trim());
						user.setNombre(nextLine[3].trim() + " " + nextLine[4].trim());
						user.setTipoUsuario(tipU);//SI EXISTE RUC EN LA BD ENTONCES EL Tip.Usu SERA E CASO CONTRARIO SERA C 
													
						if(nextLine[5].trim().equalsIgnoreCase("1")){estado="Y";}
						user.setIsActive(estado);
						
						user.setEmail("");
						
						lstuser.add(user);
	
					}
						LOGGER.info("### Se encontraron: "+ lstuser.size() + " usuarios en el archivo: "+archivosUsers[i].getName());
						guardarUsuarios(lstuser);
						reader.close();						
						FileUtil.moverArchivo(emitter.get(0).getPathReception(),archivosUsers[i]);
				}else{ LOGGER.error("El sistema no puede encontrar el archivo especificado: "+file); }
			}
		}
		

	} catch (Exception e) {
		e.printStackTrace();
	}
	
}


public void downloadFileUserFtp(Emitter emitter) throws SftpException{	
	
	String path = emitter.getPathGenerated();//  directoryGlobal;
	FTPDownloader ftp = new FTPDownloader(emitter.getFtpHost(), emitter.getFtpUser(), emitter.getFtpPassword(), emitter.getFtpDirectory(), emitter.getFtpExtension());
	ftp.connect();
	ftp.downloadFileUser(path,emitter.getPathReception());//directoryUser
	ftp.disconnect();
	
}


**/
/*private static String host;
private static String user;
private static String password;
private static String pathRaiz;
private static String ext;
private static String ftpBajadosUser;	
private static String pathreception;
private static String sftpPort;
private static String directoryBackupRemote;*/



/*
private @Value("${invoicec.sftp.host}")  String host;
private @Value("${invoicec.sftp.user}")  String user;
private @Value("${invoicec.sftp.password}")  String password;
private @Value("${invoicec.sftp.pathRaiz}")  String pathRaiz;
private @Value("${invoicec.sftp.extension}")  String ext;
private @Value("${invoicec.ftp.directory.user}")  String ftpBajadosUser;	
private @Value("${invoicec.sftp.reception}")  String pathreception;
private @Value("${invoicec.sftp.port}")  String sftpPort;
private @Value("${invoicec.sftp.directory.backup.remote}")  String directoryBackupRemote;
*/
