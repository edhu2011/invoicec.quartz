package com.cimait.invoicec.scheduling;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.cimait.invoicec.entity.DocumentRetention;
import com.cimait.invoicec.entity.Emitter;
import com.cimait.invoicec.service.InvoicecDocintroService;
import com.cimait.invoicec.service.InvoicecProcessBackgroundService;
import com.cimait.invoicec.service.repository.DocumentRetentionService;
import com.cimait.invoicec.service.repository.EmitterService;
import com.cimait.invoicec.service.repository.SummaryBajaServices;
import com.cimait.invoicec.util.InfoEmpresa;


@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class SendRetentionSummaryJob extends QuartzJobBean {


	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException { generateRetentionSummary();}

	public void generateRetentionSummary() {
		try {	
			
			LOGGER.info("### Ejecutandose JOB de Generacion de Resumen de Reversiones Retenciones");
			LOGGER.info("Hora de Ejecucion: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a").format(new Date()));
			List<Emitter> emitters = emitterSrv.findAllEmiters();
			
			for (Emitter emitter : emitters) {
				
				InfoEmpresa infoEmpresa = docIntroSrv.obtieneInfoEmpresa(emitter.getIdentification());
				
				List<DocumentRetention> lstFechaEmision = documentRetentionService.obtenerFechaEmisionDeDocsEP(infoEmpresa.getRuc());
				
				int correlativo = summaryBajaServices.getCorrelativoSummary(infoEmpresa.getRuc(), "RR");
				
				for (DocumentRetention doc : lstFechaEmision) {
					List<DocumentRetention> documentosBaja = documentRetentionService.docPendientesDeBaja(doc.getFechaEmision(), doc.getRucEmisor());
					List<DocumentRetention> lstDocs = new ArrayList<DocumentRetention>();
					Date issueDate = null;
					++correlativo;
					for (DocumentRetention documento : documentosBaja) {
						
						DocumentRetention d = new DocumentRetention();
						issueDate = documento.getFechaEmision();
						d.setNumeroLegal(documento.getNumeroLegal());
						d.setTipoDocumento(documento.getTipoDocumento());
						d.setFechaEmision(documento.getFechaEmision());
						d.setMotivoBaja(documento.getMotivoBaja());
						lstDocs.add(d);
					}
					if(lstDocs.size() > 0){					
						docIntroSrv.generarResumenReversionesRetenciones(lstDocs, issueDate, String.format("%05d",correlativo), new Date(), infoEmpresa);
						procesaResumen.procesa(infoEmpresa);
					}
					
				}

			}					
						 
		} catch (Exception e)
			{
			LOGGER.error("Ocurrio un error en la ejecucion del JOB de Generacion de Resumen de Reversiones. \n" +e);
			e.printStackTrace();
			
			}
		
	}
	
	public void generateRetentionSummary(InfoEmpresa infoEmpresa) {
		try {	
			
			LOGGER.info("### Ejecutandose JOB de Generacion de Resumen de Reversiones Retenciones");
			LOGGER.info("Hora de Ejecucion: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a").format(new Date()));
						
							
			List<DocumentRetention> lstFechaEmision = documentRetentionService.obtenerFechaEmisionDeDocsEP(infoEmpresa.getRuc());
			int correlativo = summaryBajaServices.getCorrelativoSummary(infoEmpresa.getRuc(), "RR");
				
			for (DocumentRetention doc : lstFechaEmision) {
				List<DocumentRetention> documentosBaja = documentRetentionService.docPendientesDeBaja(doc.getFechaEmision(), doc.getRucEmisor());
				List<DocumentRetention> lstDocs = new ArrayList<DocumentRetention>();
				Date issueDate = null;
				++correlativo;
				for (DocumentRetention documento : documentosBaja) {
						
					DocumentRetention d = new DocumentRetention();
					issueDate = documento.getFechaEmision();
					d.setNumeroLegal(documento.getNumeroLegal());
					d.setTipoDocumento(documento.getTipoDocumento());
					d.setFechaEmision(documento.getFechaEmision());
					d.setMotivoBaja(documento.getMotivoBaja());
					lstDocs.add(d);
				}
				if(lstDocs.size() > 0){
					docIntroSrv.generarResumenReversionesRetenciones(lstDocs, issueDate, String.format("%05d",correlativo), new Date(), infoEmpresa);
					procesaResumen.procesa(infoEmpresa);
				}					
			}


		} catch (Exception e){
			LOGGER.error("Ocurrio un error en la ejecucion del JOB de Generacion de Resumen de Reversiones. \n" +e);
			e.printStackTrace();
			
		}
	}


	@Autowired
	private SummaryBajaServices summaryBajaServices;
	@Autowired
	private InvoicecDocintroService docIntroSrv;
	@Autowired
	private EmitterService emitterSrv;
	@Autowired
	private DocumentRetentionService documentRetentionService;	
	@Autowired
	private InvoicecProcessBackgroundService procesaResumen;
	private static Logger LOGGER = Logger.getLogger(SendRetentionSummaryJob.class);

	
	
	
	
	
	
	
}
