package com.cimait.invoicec.scheduling;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.cimait.invoicec.entity.Document;
import com.cimait.invoicec.entity.Emitter;
import com.cimait.invoicec.service.InvoicecDocintroService;
import com.cimait.invoicec.service.InvoicecProcessBackgroundService;
import com.cimait.invoicec.service.repository.DocumentService;
import com.cimait.invoicec.service.repository.EmitterService;
import com.cimait.invoicec.service.repository.ReceiptSummaryService;
import com.cimait.invoicec.util.DateUtil;
import com.cimait.invoicec.util.InfoEmpresa;


@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class SendReceiptSummaryVoidedJob extends QuartzJobBean
{
	
	@Override
	protected void executeInternal(JobExecutionContext context)	throws JobExecutionException { generateReceiptSummaryVoided();}

	public void generateReceiptSummaryVoided(){
		try {
				LOGGER.info("### Ejecutandose JOB de Generacion de Resumen de Boleta de Bajas");
				LOGGER.info("Hora de Ejecucion: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a").format(new Date()));
				//Obtenemos todos los emisores registrados en la base de datos
				List<Emitter> emitters = emitterSrv.findAllEmiters();
				for (Emitter emitter : emitters) {
				
					//Obtenemos informacion de cada emisor
					InfoEmpresa infoEmpresa = docIntroSrv.obtieneInfoEmpresa(emitter.getIdentification());
					
						
					
					//Obtenemos boletas de baja segun ruc-emisor ordenados por fecha de emision				
					List<Document> lstFechaEmision = documentService.obtenerFechaEmisionDeBoletasPB(infoEmpresa.getRuc());	
					int correlativo = receiptSummaryService.getCorrelativoSummary(infoEmpresa.getRuc());									
					for (Document doc : lstFechaEmision) {
						
						//Obtenemos boletas segun fecha de emision
						List<Document> boletas = documentService.docBoletasPendientesDeBaja(doc.getFechaEmision(), infoEmpresa.getRuc());
						LOGGER.info("Total de boletas PB encontradas: "+boletas.size());
						++correlativo;					
						
							
							LOGGER.info("Generando resumen: " +infoEmpresa.getRuc() + "-RC-" + DateUtil.formatDateSummaryFileName(new Date()) +  "-" + String.format("%05d",correlativo) + ".txt");
							
							//proceso 01: generar los resumenes
							docIntroSrv.generarResumen(boletas, boletas.get(0).getFechaEmision(), String.format("%05d",correlativo), new Date(), infoEmpresa);
							
							 //proceso 02: procesar cada resumen generado
							procesaResumen.procesa(infoEmpresa);

						
					}
					
	
				}			
			
		} catch (Exception e)
			{
				LOGGER.error("Ocurrio un error en la ejecucion del JOB de Generacion de Resumen de Baja de Facturas. \n" +e);
				e.printStackTrace();
			
			}
	}
	

	
	
	@Autowired
	private InvoicecDocintroService docIntroSrv;
	@Autowired
	private DocumentService documentService;
	@Autowired
	private EmitterService emitterSrv;
	@Autowired
	private InvoicecProcessBackgroundService procesaResumen;
	@Autowired
	private ReceiptSummaryService receiptSummaryService;
	
	private static Logger LOGGER = Logger.getLogger(SendReceiptSummaryVoidedJob.class);

}
