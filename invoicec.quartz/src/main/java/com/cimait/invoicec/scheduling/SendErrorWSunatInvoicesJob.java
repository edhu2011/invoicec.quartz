package com.cimait.invoicec.scheduling;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cimait.invoicec.entity.Document;
import com.cimait.invoicec.entity.DocumentError;
import com.cimait.invoicec.entity.DocumentRetention;
import com.cimait.invoicec.entity.DocumentRetentionError;
import com.cimait.invoicec.service.InvoicecDocintroService;
import com.cimait.invoicec.service.InvoicecRetentionService;
import com.cimait.invoicec.service.repository.DocumentRetentionService;
import com.cimait.invoicec.service.repository.DocumentService;
import com.cimait.invoicec.util.FileUtil;
import com.cimait.invoicec.util.InfoEmpresa;


@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class SendErrorWSunatInvoicesJob extends QuartzJobBean {
	
	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException { 
		
		SendInvoiceError();
		
	}
	
	@Transactional
	private void SendInvoiceError(){
		try {
			
				
				LOGGER.info("### Ejecutandose JOB de Reenvio de Documentos por Error WSSunat");
				LOGGER.info("Hora de Ejecucion: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a").format(new Date()));
			
				//FACTURAS
				List<Document> ltsdocErrorWs = documentService.findAllDocumentErrorWS();			
				for (Document doc : ltsdocErrorWs) {
					InfoEmpresa infoEmpresa = docIntroSrv.obtieneInfoEmpresa(doc.getRuc());		
					DocumentError error = documentService.findLastDocumentErrorDetailWS(doc.getId());
					
					if(error != null && error.getMsg().toUpperCase().contains("ERROR EN WEB SERVICE DE SUNAT"))
					{
						LOGGER.info("Enviando FACTURAS por ERROR DE SUNAT el documento ==" + doc.getNombreArchivo());
						//FileUtil.setFileName(doc.getNombreArchivo());
						LOGGER.info("Reenviando Factura " + doc.getNumeroLegal());
						if((doc.getTipoDocumento().equals("07") || doc.getTipoDocumento().equals("08")) && doc.getDocumentoModificado().startsWith("F")){
							Document factura = documentService.obtenerDocumentoModificado(doc.getDocumentoModificado().trim(), "01",doc.getRuc());
							if(factura != null && factura.getStatus().equals("AT")){
										docIntroSrv.processPendingInvoice(infoEmpresa, doc);
							}
							else{
									if (factura != null && !factura.getStatus().equals("AT")){
										docIntroSrv.saveStatusError(doc, "La factura relacionada "+doc.getDocumentoModificado()+" NO esta AUTORIZADA");
										LOGGER.info("La factura relacionada "+doc.getDocumentoModificado()+" NO esta AUTORIZADA");
										
									}
									else{
										docIntroSrv.saveStatusError(doc, "La factura relacionada NO esta REGISTRADA EN LA BD con datos: numero = "+doc.getDocumentoModificado()+", tipo = 01, ruc = "+doc.getRuc());											
										LOGGER.info("La factura relacionada NO esta REGISTRADA EN LA BD con datos: numero = "+doc.getDocumentoModificado()+", tipo = 01, ruc = "+doc.getRuc());
										
									}
							}
						}
						else{
							docIntroSrv.processPendingInvoice(infoEmpresa, doc);
						}
						
						
					}else{
						LOGGER.info("NO se encontraron FACTURAS con ERROR EN WEB SERVICE DE SUNAT");
					}
					
				}//Termino del recorrido de facturas erroneas
				
				Thread.sleep(5000);
		
			
				//RETENCIONES
				List<DocumentRetention> ltsdocErrorWsR = documentRetentionService.findAllDocumentErrorWS();
				
				for (DocumentRetention docRet : ltsdocErrorWsR) {
					
					InfoEmpresa infoEmpresaRet = docIntroSrv.obtieneInfoEmpresa(docRet.getRucEmisor());	
					DocumentRetentionError erros = documentRetentionService.findLastDocumentErrorDetailWS(docRet.getId());
					
					if(erros != null && erros.getMsg().toUpperCase().contains("ERROR EN WEB SERVICE DE SUNAT"))
					{
						LOGGER.info("Enviando RETENCIONES por ERROR DE SUNAT el documento ==" + docRet.getNombreArchivo());						
						LOGGER.info("Reenviando RETENCION " + docRet.getNumeroLegal());
						
						retSrv.envioRetencion(infoEmpresaRet, docRet);
					}
					else{
						LOGGER.info("NO se encontraron RETENCIONES con ERROR EN WEB SERVICE DE SUNAT");
					}
					
				}//Termino del recorrido de retenciones erroneas
				

				
				LOGGER.info("### FINAL DEL JOB de Reenvio de Documentos por Error WSSunat");
		
		} catch (Exception e) {
			LOGGER.error(e);
		}
		
		
	}
	
	
	
	
	@Autowired
	private DocumentService documentService;
	@Autowired
	private DocumentRetentionService documentRetentionService;		
	@Autowired
	private InvoicecDocintroService docIntroSrv;
	@Autowired
	private InvoicecRetentionService retSrv;
	
	private static Logger LOGGER = Logger.getLogger(SendErrorWSunatInvoicesJob.class);
}
