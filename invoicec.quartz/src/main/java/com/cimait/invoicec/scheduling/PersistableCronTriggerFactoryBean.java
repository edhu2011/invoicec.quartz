package com.cimait.invoicec.scheduling;

import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailAwareTrigger;

public class PersistableCronTriggerFactoryBean extends CronTriggerFactoryBean {

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		getJobDataMap().remove(JobDetailAwareTrigger.JOB_DETAIL_KEY);
	}

}
