package com.cimait.invoicec.scheduling;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cimait.invoicec.entity.Emitter;
import com.cimait.invoicec.entity.Summary;
import com.cimait.invoicec.entity.SummaryBaja;
import com.cimait.invoicec.service.InvoicecDocintroService;
import com.cimait.invoicec.service.InvoicecRetentionService;
import com.cimait.invoicec.service.repository.EmitterService;
import com.cimait.invoicec.service.repository.ReceiptSummaryService;
import com.cimait.invoicec.service.repository.SummaryBajaServices;
import com.cimait.invoicec.service.util.ReceiptSummaryFilter;
import com.cimait.invoicec.service.util.SummaryBajaFilter;
import com.cimait.invoicec.util.InfoEmpresa;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class SendTicketSummaryJob extends QuartzJobBean{

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException { sendTicketSumary(); sendTicketSummaryBaja(); }

	@Transactional
	public void sendTicketSumary(){
		
		LOGGER.info("### Ejecutandose JOB Envio de Resumen Y Consulta de Ticket - Resumen de Boleta:");
		LOGGER.info("Hora de Ejecucion: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a").format(new Date()));
		
		List<Emitter> emitters = emitterSrv.findAllEmiters();
		ReceiptSummaryFilter receiptSummaryFilter = null;
		List<Summary> lstSummaryStatus = null;
		InfoEmpresa infoEmpresa = null;
		String status = null;
		try {
					
			for(Emitter lst : emitters){
				infoEmpresa = docIntroSrv.obtieneInfoEmpresa(lst.getIdentification());
				receiptSummaryFilter = new ReceiptSummaryFilter();
				receiptSummaryFilter.setRucCompany(lst.getIdentification());
				
				receiptSummaryFilter.setStatus("SD");
				lstSummaryStatus = receiptSummaryService.findAllByFilter(receiptSummaryFilter);
				LOGGER.info("####################################################################################");
				LOGGER.info("Consulta de Ticket de Resumen de Boletas para la empresa: " + infoEmpresa.getRazonSocial() + " - " + lst.getIdentification());
				LOGGER.info("Cantidad de documentos para la Consulta Ticket: "+ lstSummaryStatus.size());
				LOGGER.info("####################################################################################");
				LOGGER.info("");
				for(Summary lstSummary : lstSummaryStatus){
					status = docIntroSrv.obtenerStatusResumen(lstSummary, infoEmpresa);
					//if(status != null && status.equalsIgnoreCase("98")) docIntroSrv.generarResumenDeBoletas(lstSummary.getFechaEmisionDocumentos(), infoEmpresa);
					if(status != null && status.equalsIgnoreCase("98")) sendReceiptSummaryJob.generateReceiptSummary(lstSummary.getFechaEmisionDocumentos(), infoEmpresa);
				}
				LOGGER.info(" ###### Esperando 5 segundos para realizar el siguiente proceso del JOB - Resumen de Boletas ##### ");
				Thread.sleep(5000);
				LOGGER.info("####################################################################################");
				LOGGER.info("Envio de Resumen de Boletas para la empresa: " + infoEmpresa.getRazonSocial() + " - " + lst.getIdentification());
				LOGGER.info("Cantidad de documentos para el Envio de Resumen: "+ lstSummaryStatus.size());
				LOGGER.info("####################################################################################");
				LOGGER.info("");
				lstSummaryStatus = null;
				receiptSummaryFilter.setStatus("CR");				
				lstSummaryStatus = receiptSummaryService.findAllByFilter(receiptSummaryFilter);
				for(Summary lstSummary : lstSummaryStatus){
					try {
						docIntroSrv.reenviarResumen(lstSummary, lst.getIdentification());
					} catch (Exception e) { 
						LOGGER.info(e);
						e.printStackTrace(); 
					
					}
	
				}
				
			}
			
		} catch (Exception e) { 
			LOGGER.error("El proceso del JOB: Envio de resumen y consulta de ticket NO CONCLUYO CORRECTAMENTE: " + e);
			e.printStackTrace();
			
		}
		
		LOGGER.info("################## Fin Job Envio de Resumen Y Consulta de Ticket - Resumen de Boleta ##################");
		
	}
	
	@Transactional
	public void sendTicketSummaryBaja(){
		
		LOGGER.info("### Ejecutandose JOB Envio de Resumen de Baja Y Consulta de Ticket de Baja - Resumen de Bajas:");
		LOGGER.info("Hora de Ejecucion: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a").format(new Date()));
		
		List<Emitter> emitters = emitterSrv.findAllEmiters();
		SummaryBajaFilter summaryBajaFilter = null;
		List<SummaryBaja> lstSummaryBajaStatus = null;
		InfoEmpresa infoEmpresa = null;
		String status = null;

		try {
			
			for(Emitter lst : emitters){
				infoEmpresa = docIntroSrv.obtieneInfoEmpresa(lst.getIdentification());
				summaryBajaFilter = new SummaryBajaFilter();
				summaryBajaFilter.setRucCompany(lst.getIdentification());
				
				summaryBajaFilter.setStatus("SD");
				lstSummaryBajaStatus = summaryBajaServices.findAllByFilter(summaryBajaFilter);
												
				LOGGER.info("####################################################################################");
				LOGGER.info("Consulta de Ticket de Resumen de Baja para la empresa: " + infoEmpresa.getRazonSocial() + " - " + lst.getIdentification());
				LOGGER.info("Cantidad de documentos para la Consulta Ticket: "+ lstSummaryBajaStatus.size());
				LOGGER.info("####################################################################################");
				LOGGER.info("");
				for(SummaryBaja lstSummaryBaja : lstSummaryBajaStatus){
					
					if(lstSummaryBaja.getIdentificadorResumen().trim().startsWith("RA")){
						LOGGER.info("########## Baja de comprobantes tipo: FE-BE-NCE-NDE ##########");
						status = docIntroSrv.obtenerStatusResumenBaja(lstSummaryBaja, infoEmpresa);						
						//if(status != null && status.equalsIgnoreCase("98")) docIntroSrv.generarComunicacionDeBaja(infoEmpresa);
						if(status != null && status.equalsIgnoreCase("98")) generatedComunicationJob.generateComunicacionBaja(infoEmpresa);
					}
					
					if(lstSummaryBaja.getIdentificadorResumen().trim().startsWith("RR")){
						String tipoDoc = docRetSrv.getDocumentTypeFromSummaryBaja(lstSummaryBaja.getIdentificadorResumen(), lstSummaryBaja.getRucEmisor());
						if(tipoDoc != null && tipoDoc.equalsIgnoreCase("20")) {
							LOGGER.info("########## Baja de Retenciones ##########");
							status = docRetSrv.obtenerStatusResumenBaja(lstSummaryBaja, infoEmpresa);
							//if(status != null && status.equalsIgnoreCase("98")) docRetSrv.generarResumenReversiones(infoEmpresa);
							if(status != null && status.equalsIgnoreCase("98")) sendRetentionSummaryJob.generateRetentionSummary(infoEmpresa);
						}
						if(tipoDoc != null && tipoDoc.equalsIgnoreCase("40")) {
							/*status = docRetSrv.obtenerStatusResumenBaja(lstSummaryBaja, infoEmpresa);
							if(status != null && status.equalsIgnoreCase("98")) docRetSrv.generarResumenReversiones(infoEmpresa);*/
						}
																		
					}

				}
				LOGGER.info(" ###### Esperando 5 segundos para realizar el siguiente proceso del JOB - Resumen de bajas##### ");
				Thread.sleep(5000);
				LOGGER.info("####################################################################################");
				LOGGER.info("Envio de Resumen de Bajas para la empresa: " + infoEmpresa.getRazonSocial() + " - " + lst.getIdentification());
				LOGGER.info("Cantidad de documentos para el Envio de Resumen de bajas: "+ lstSummaryBajaStatus.size());
				LOGGER.info("####################################################################################");
				LOGGER.info("");
				lstSummaryBajaStatus = null;
				summaryBajaFilter.setStatus("CR");
				lstSummaryBajaStatus = summaryBajaServices.findAllByFilter(summaryBajaFilter);
				for(SummaryBaja lstSummaryBaja : lstSummaryBajaStatus){
					try {
						if(lstSummaryBaja.getIdentificadorResumen().trim().startsWith("RR")){
							docRetSrv.reenviarResumenReversion(lst.getIdentification(), lstSummaryBaja.getIdentificadorResumen(), lstSummaryBaja.getNombreArchivo());
						}
						if(lstSummaryBaja.getIdentificadorResumen().trim().startsWith("RA")){
							docIntroSrv.reenviarComunicacionBaja(lst.getIdentification(), lstSummaryBaja.getIdentificadorResumen(), lstSummaryBaja.getNombreArchivo());
						}
					} catch (Exception e) { LOGGER.info(e); e.printStackTrace(); }
	
				}
				
			}
			
		} catch (Exception e) { 
			LOGGER.error("El proceso del JOB: Envio de resumen y consulta de ticket NO CONCLUYO CORRECTAMENTE: " + e);
			e.printStackTrace();
			
		}
		
		LOGGER.info("################## Fin JOB Envio de Resumen de Baja Y Consulta de Ticket de Baja - Resumen de Bajas ##################");
		
	}
	
	@Autowired
	private InvoicecRetentionService docRetSrv;	
	@Autowired
	private EmitterService emitterSrv;
	@Autowired
	private ReceiptSummaryService receiptSummaryService;
	@Autowired
	private SummaryBajaServices summaryBajaServices;
	@Autowired
	private InvoicecDocintroService docIntroSrv;
	
	//inyectando jobs
	@Autowired
	private SendReceiptSummaryJob sendReceiptSummaryJob; //GeneratedComunicationJob
	@Autowired
	private GeneratedComunicationJob generatedComunicationJob;
	@Autowired
	private SendRetentionSummaryJob sendRetentionSummaryJob;
	
	private static Logger LOGGER = Logger.getLogger(SendTicketSummaryJob.class);
}
