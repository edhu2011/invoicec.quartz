package com.cimait.invoicec.scheduling;

import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.cimait.invoicec.service.InvoicecDocintroService;
import com.cimait.invoicec.service.InvoicecSunatService;
import com.cimait.invoicec.util.FTPDownloader;
import com.cimait.invoicec.util.FileUtil;
import com.cimait.invoicec.util.InfoEmpresa;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ReadJuliacaFtpInvoicesJob extends QuartzJobBean{
	
		public void readJuliacaFtpInvoices() {
					InfoEmpresa infEmp = new InfoEmpresa();
					try {infEmp = docIntroSrv.obtieneInfoEmpresa(ruc);}catch(Exception ex){ex.printStackTrace();	}
										
					LOGGER.info("######### Ejecutandose JOB de Lectura de FTP #########");
					LOGGER.info("ftpHost: " + infEmp.getFtpHost());
					LOGGER.info("ftpUser: " + infEmp.getFtpUser());
					LOGGER.info("ftpDirectory: " + infEmp.getFtpDirectory());
					LOGGER.info("ftpExtension: " + infEmp.getFtpExtension());					
					LOGGER.info("Hora de Ejecucion: " + new Date());					
					FTPDownloader ftp = new FTPDownloader(infEmp.getFtpHost(), infEmp.getFtpUser(), infEmp.getFtpPassword(), infEmp.getFtpDirectory(), infEmp.getFtpExtension());
					ftp.connect();
					ftp.downloadFiles(infEmp.getPathFtpRecibido());
					ftp.disconnect();
					LOGGER.info("Moviendo Archivos leidos del FTP a carpeta GENERADOS ");
					FileUtil.moverArchivos(infEmp.getPathFtpRecibido(), infEmp.getDirGenerado());
					LOGGER.info("### Fin de ejecucion del JOB de Lectura de FTP.");
					
					
		}
	
		@Override
		protected void executeInternal(JobExecutionContext context) throws JobExecutionException { readJuliacaFtpInvoices();  }

		@Autowired
		private InvoicecDocintroService docIntroSrv;
		@Autowired
		private InvoicecSunatService sunatSrv;
		private static Logger LOGGER = Logger.getLogger(ReadJuliacaFtpInvoicesJob.class);
		private @Value("${invoicec.emitter.ruc}") String ruc;
		
}
