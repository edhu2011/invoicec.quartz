package com.cimait.invoicec.util;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringUtil {

    private static final ApplicationContext context;
    private static final Logger LOGGER = Logger.getLogger(SpringUtil.class);
    
    static {
        try {
        	System.out.println("nosotros cargamos el contexto: " + "classpath*:com/cimait/invoicec/core/spring/quartz-application-context.xml");
        	context = new ClassPathXmlApplicationContext("classpath*:com/cimait/invoicec/core/spring/quartz-application-context.xml");
    
        } catch (Throwable ex) {
        			LOGGER.error("Error al cargar el context de spring\n",ex);
            		System.err.println("Initial SessionFactory creation failed." + ex);
            		throw new ExceptionInInitializerError(ex);
        }
    }
  
    public static ApplicationContext getApplicationContext() {
        return context;
    }
}